# Table Objects

### Introduction
*An ORM focused on flexibility for the developer*

Table Objects doesn't want you to conform to it, it wants to conform to your needs and workflow.

### Installation
The best way to install is by composer:
```
composer require roll6/table-objects
```

### [Setup](/docs/)
