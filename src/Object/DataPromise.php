<?php

namespace TableObjects\Object {

    class DataPromise
    {
        private $_class;
        private $_primaryKey;

        public function __construct(string $_class, $_primaryKey)
        {
            $this->_class = $_class;
            $this->_primaryKey = $_primaryKey;
        }

        public function &getPromisedKey()
        {
            return $this->_primaryKey;
        }

        public function &getPrimary()
        {
            return $this->getPromisedKey();
        }

        public function getClass(): string
        {
            return $this->_class;
        }

        /**
         * @return TableObject
         */
        public function fulfill(): TableObject
        {
            return $this->_class::get($this->_primaryKey);
        }

        public function __call($name, $arguments)
        {
            return $this->fulfill()->$name(...$arguments);
        }

        public function __get($name)
        {
            return $this->fulfill()->$name;
        }

        public function __set($name, $value)
        {
            return $this->fulfill()->$name = $value;
        }

        public function __isset($name)
        {
            return isset($this->fulfill()->$name);
        }
    }

}