<?php

namespace TableObjects\Object\Exception {

    class DataAttributionModeException extends \Exception
    {}
}