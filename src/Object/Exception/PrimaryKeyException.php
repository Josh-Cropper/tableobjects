<?php
declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: The_Evil_Dice
 * Date: 25/11/2018
 * Time: 02:42
 */
namespace TableObjects\Object\Exception {

    class PrimaryKeyException extends \Exception {

    }

}