<?php
namespace TableObjects\Object\Exception {

    use Throwable;

    class NoDatabaseException extends \Exception
    {

        public function __construct($message = 'No PDO Connection Provided', $code = 0, Throwable $previous = null)
        {
            parent::__construct($message, $code, $previous);
        }
    }

}