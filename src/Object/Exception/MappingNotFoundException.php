<?php

namespace TableObjects\Object\Exception {

    class MappingNotFoundException extends \Exception
    {}

}