<?php

namespace TableObjects\Object {

    use Exception;
    use TableObjects\Object\Exception\MappingNotFoundException;
    use TableObjects\Object\Exception\NonExistantObjectException;
    use TableObjects\Object\Exception\PrimaryKeyException;
    use TableObjects\PDOWrapper\QueryBuilder\QueryBuilder;

    class DataLoader
    {
        private $_caller;

        public function __construct(TableObject $_caller)
        {
            $this->_caller = $_caller;
        }

        /**
         * @param int|string $primaryKey
         * @return array
         * @throws MappingNotFoundException
         * @throws NonExistantObjectException
         * @throws PrimaryKeyException
         */
        public function load($primaryKey): array
        {
            $binds = $selects = $primary = $vars = [];
            foreach ($this->_caller::getMappings() as $param => $mapping) {
                if (isset($mapping['primary']) && (bool)(int)$mapping['primary']) {
                    $this->cast($vars[$param],$param, [$param => $primaryKey]);
                    $binds[':_primary'] = $primaryKey;
                    $primary = [
                        'table' => $this->_caller::getTable(),
                        'col'   => $mapping['db']['col'] ?? $param
                    ];
                    continue;
                }

                $table = $mapping['db']['table'] ?? $this->_caller::getTable();

                $cols = [
                    $param => $mapping['db']['col'] ?? $param
                ];
                $noAssign = [];
                if (isset($mapping['loadMethods'])) {
                    foreach ($mapping['loadMethods'] as $method) {
                        if (isset($method['methodArgs'])) {
                            foreach ($method['methodArgs'] as $arg) {
                                if (isset($arg['type']) && $arg['type'] === 'col') {
                                    if (!isset($cols[$arg['value']])) {
                                        $cols[$arg['value']] = $arg['value'];
                                        $noAssign[] = $arg['value'];
                                    }
                                }
                            }
                        }
                    }
                }

                if (($this->_caller::getParamType($param) ?? 'string') === 'dynamic') {
                    if (!isset($mapping['dynamicType']['col']) || empty($mapping['dynamicType']['col'])) {
                        throw new MappingNotFoundException("Dynamic mapping not set for {$param} in {$this->_caller::getClass()}");
                    }
                    if (!isset($cols[$mapping['dynamicType']['col']])) {
                        $cols[$mapping['dynamicType']['col']] = $mapping['dynamicType']['col'];
                        $noAssign[] = $mapping['dynamicType']['col'];
                    }
                }

                $whereCols = [];
                if (isset($mapping['db']['whereCols'])) {
                    foreach ($mapping['db']['whereCols'] as $whereCol) {
                        $whereCols[] = [
                            'table' => $table,
                            'col'   => $whereCol
                        ];
                    }
                } else {
                    $whereCols[] = $primary;
                }
                $orderBys = [];
                if (isset($mapping['db']['orderBy'])) {
                    foreach ($mapping['db']['orderBy'] as $orderBy) {
                        $orderBys[] = [
                            'clause' => "`{$table}`.{$orderBy['col']}",
                            'dir' => strtolower($orderBy['dir'] ?? 'ASC')
                        ];
                    }
                } elseif (isset($mapping['db']['handleOrdering']['col'])) {
                    $orderBys[] = [
                        'clause' => "`{$table}`.{$mapping['db']['handleOrdering']['col']}",
                        'dir' => 'asc'
                    ];
                    $noAssign[] = $mapping['db']['handleOrdering']['col'];
                    $cols[$mapping['db']['handleOrdering']['col']] = $mapping['db']['handleOrdering']['col'];
                }

                $match = false;
                foreach ($selects as &$select) {
                    if (($select['table'] === $table)
                        && ($select['whereCols'] === $whereCols)
                        && $select['orderBy'] === $orderBys) {
                        $select['cols'] = array_unique(array_merge($select['cols'], $cols));
                        $select['noAssign'] = array_diff(
                            array_unique(array_merge($select['noAssign'], $noAssign)),
                            array_keys($select['cols'])
                        );
                        $match = true;
                        break;
                    }
                }
                unset($select);

                if (!$match) {
                    $selects[] = [
                        'table'     => $table,
                        'cols'      => $cols,
                        'noAssign'  => $noAssign,
                        'whereCols' => $whereCols,
                        'orderBy'   => $orderBys
                    ];
                }

            }
            if (!isset($binds[':_primary']) || empty($binds[':_primary'])) {
                throw new PrimaryKeyException('Primary key empty');
            }

            foreach ($selects as $select) {
                $qb = (new QueryBuilder($this->_caller::getDb()))
                    ->select()
                    ->from($select['table']);

                foreach ($select['cols'] as $param => $col) {
                    $qb->col($col, $select['table'], $param);
                }

                foreach ($select['whereCols'] as $whereCol) {
                    $qb->where($whereCol['col'], $whereCol['table'], '=', $binds[':_primary']);
                }

                foreach ($select['orderBy'] as $order) {
                    $qb->orderBy($order['clause'], $order['dir']);
                }

                $sql = $qb->exec();
                if ($sql->errorCode() !== '00000') {
                    throw new Exception("SQL Error ({$sql->errorCode()}): {$sql->errorInfo()[2]}");
                }

                $return = $sql->fetchAll(\PDO::FETCH_ASSOC);

                if (empty($return) && ($qb->getFrom()['table'] === $this->_caller::getTable() && $qb->onlyWhere($this->_caller::getPrimaryCol(), $this->_caller::getTable(), '=', $primaryKey))) {
                    throw new NonExistantObjectException("{$this->_caller::getClass()}({$primaryKey}) does not exist.");
                }

                foreach ($return as $row) {
                    foreach ($row as $param => $value) {
                        if (!in_array($select['cols'][$param], $select['noAssign'], true)) {
                            $this->cast($vars[$param], $param, $row);
                        }
                    }
                }
            }

            return $vars;
        }

        /**
         * @param $assignee
         * @param string $param
         * @param array $row
         * @param string|NULL $forceType
         * @throws MappingNotFoundException
         * @throws PrimaryKeyException
         */
        private function cast(&$assignee, string $param, array $row, string $forceType = NULL): void
        {
            $type = $forceType ?? $this->_caller::getParamType($param);
            $is_array = $this->_caller::getParamIsArray($param);

            switch ($type) {
                case 'int':
                    if ($is_array) {
                        $assignee[] = (int)$row[$param];
                    } else {
                        $assignee = (int)$row[$param];
                    }
                    break;
                case 'double':
                    if ($is_array) {
                        $assignee[] = (double)$row[$param];
                    } else {
                        $assignee = (double)$row[$param];
                    }
                    break;
                case 'float':
                    if ($is_array) {
                        $assignee[] = (float)$row[$param];
                    } else {
                        $assignee = (float)$row[$param];
                    }
                    break;
                case 'bool':
                    if ($is_array) {
                        $assignee[] = (bool)$row[$param];
                    } else {
                        $assignee = (bool)$row[$param];
                    }
                    break;
                case 'string':
                    if ($is_array) {
                        $assignee[] = (string)$row[$param];
                    } else {
                        $assignee = (string)$row[$param];
                    }
                    break;
                case 'json':
                    if ($is_array) {
                        $assignee[] = json_decode($row[$param], true);
                    } else {
                        $assignee = json_decode($row[$param], true);
                    }
                    break;
                case 'dynamic':
                    if (empty($this->_caller::getMappings($param)['dynamicType']['col'] ?? [])) {
                        throw new MappingNotFoundException("Dynamic mapping not set for {$param} in {$this->_caller::getClass()}");
                    }

                    $type = preg_match(
                        "/^\[.*]$/",
                        $row[$this->_caller::getMappings($param)['dynamicType']['col']]
                    ) ? $row[$this->_caller::getMappings($param)['dynamicType']['col']]
                        : "[{$row[$this->_caller::getMappings($param)['dynamicType']['col']]}]";

                    $this->cast($assignee, $param, $row, $type);
                    return;
                default:
                    if($row[$param] === NULL
                        && ($type === \DateTime::class || ($this->_caller::getMappings($param)['allowNull']??false))) {
                        if ($is_array) {
                            $assignee[] = NULL;
                        } else {
                            $assignee = NULL;
                        }
                        break;
                    }

                    /** @var $type TableObject */
                    if (is_subclass_of($type, TableObject::class)) {
                        try {
                            if ($is_array) {
                                $assignee[] = $type::promise($row[$param]);
                            } else {
                                $assignee = $type::promise($row[$param]);
                            }
                        } catch (PrimaryKeyException $e) {
                            if (!empty($row[$param])) {
                                throw $e;
                            }
                        }
                        break;
                    }
                    try {
                        $obj = new $type($row[$param]);
                        if ($this->_caller::getParamLoadMethods($param)) {
                            foreach ($this->_caller::getParamLoadMethods($param) as $method) {
                                if (!isset($method['methodName']) || empty($method['methodName'])) {
                                    break;
                                }
                                $args = [];
                                foreach ($method['methodArgs'] as $arg) {
                                    if (!isset($arg['type'], $arg['value'])
                                        || ($arg['type'] === 'col' && !$this->_caller::isValidArrayKey($arg['value']))
                                    ) {
                                        break 2;
                                    }

                                    switch ($arg['type']) {
                                        case 'col':
                                            $args[] = $row[$arg['value']];
                                            break;
                                        case 'value':
                                            $args[] = $arg['value'];
                                            break;
                                        default:
                                            break 3;
                                    }
                                }
                                $obj->{$method['methodName']}(...$args);
                            }
                        }
                        if ($is_array) {
                            $assignee[] = $obj;
                        } else {
                            $assignee = $obj;
                        }
                    } catch (PrimaryKeyException $e) {
                    }
                    break;
            }
        }

    }

}