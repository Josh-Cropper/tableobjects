<?php
declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: josh
 * Date: 18/08/18
 * Time: 16:47
 */

namespace TableObjects\Object {

    use PDO;
    use TableObjects\Object\Exception\MappingNotFoundException;
    use TableObjects\Object\Exception\NonExistantObjectException;
    use TableObjects\Object\Exception\PrimaryKeyException;
    use Exception;
    use TableObjects\PDOWrapper\PDOWrapper;
    use TableObjects\PDOWrapper\QueryBuilder\QueryBuilder;
    use TableObjects\PDOWrapper\QueryBuilder\WhereGroup;

    /**
     * Class TableObject
     * @package TableObjects\Object
     * @method postConstruct($primaryKey)
     * @method preDelete()
     * @method postDelete()
     * @method preSave()
     * @method postSave()
     * @method postLoad()
     * @method preLoad()
     */
    abstract class TableObject
    {

        /**
         * @var PDOWrapper $_db
         */
        private static $_db;
        private static $_objects = [];
        private static $_loading = [];

        protected static $table;

        public const TYPES = ['int', 'double', 'float', 'string', 'bool', 'json'];

        public const BELONGS_TO_ONE = 0;
        public const BELONGS_TO_MANY = 1;
        public const CONTAINS_ONE = 2;
        public const CONTAINS_MANY = 3;

        protected $uniq;

        /**
         * @var array $mappings
         * Tells the mapping system how data should be loaded
         * See below for an example
         */
        protected static $mappings;
        private static $_mergedMappings = [];

        /**
         *  $_mappings example
         *  [
         *      param => [
         *          'primary'   => bool (optional, defaults to false),
         *          'type'      => string|array (optional, defaults to string) (int|double|float|string|bool|json|dynamic|ClassName),
         *          'allowNull' => bool (optional, defaults to false),
         *          'dynamicType' => array (optional) [
         *              'col' => string,
         *          ],
         *          'deleteTableObject' => bool (directly linked TableObject to be deleted upon this deletion, optional, defaults to false)
         *          'saveFunction' => Removed temporarily,
         *          'saveMethod' => string (method that is called on the param before saving, optional),
         *          'saveArguments' => array (arguments passed by splat operator to the saveMethod if specified, optional),
         *          'loadMethods' array (these are called in the order you specify them, optional) => [
         *              [
         *                  'methodName' => string (method that is called on the object after instantiation),
         *                  'methodArgs' => array (optional) [
         *                      [
         *                          'type' => string (col|static),
         *                          'value' =>
         *                      ]
         *                  ]
         *              ]
         *          ],
         *          'db'        => [
         *              'table' => string (table name, optional, defaults to class name),
         *              'col' => string (optional, defaults to param name) (column name),
         *              'whereCols' (optional) => [
         *                  string (column name)
         *              ],
         *              'orderBy' (optional) => [
         *                  [
         *                      'col' => string (column name),
         *                      'dir' => string (order direction, optional, defaults to asc)
         *                  ]
         *              ],
         *              handleOrdering (optional, cannot be used in conjunction with orderBy) => [
         *                  'col' => (string)
         *              ]
         *          ]
         *      ]
         */

        protected static $existsIn;

        /**
         * @param PDO|PDOWrapper $_db
         */
        final public static function init($_db): void
        {
            if (self::$_db === NULL) {
                if ($_db instanceof PDO) {
                    self::$_db = new PDOWrapper($_db);
                    return;
                }
                if ($_db instanceof PDOWrapper) {
                    self::$_db = $_db;
                    return;
                }
                throw new \InvalidArgumentException('$_db must be instance of PDO or PDOWrapper');
            }
        }

        /**
         * @return PDOWrapper
         */
        final public static function getDb(): PDOWrapper
        {
            return self::$_db;
        }

        /**
         * @return static[]
         */
        final protected static function &getObjects(): array
        {
            if (!isset(self::$_objects[static::class])) {
                self::$_objects[static::class] = [];
            }
            return self::$_objects[static::class];
        }

        /**
         * @return array
         */
        final protected static function getLoading(): array
        {
            if (!isset(self::$_loading[static::class])) {
                self::$_loading[static::class] = [];
            }
            return self::$_loading[static::class];
        }

        /**
         * @param $primary
         * @throws PrimaryKeyException
         */
        final protected static function setLoading($primary): void
        {
            if (!self::isValidArrayKey($primary)) {
                throw new PrimaryKeyException('');
            }
            if (!isset(self::$_loading[static::class][$primary])) {
                self::$_loading[static::class][$primary] = 1;
            }
        }

        /**
         * @param $primary
         * @throws PrimaryKeyException
         */
        final protected static function unsetLoading($primary): void
        {
            if (!self::isValidArrayKey($primary)) {
                throw new PrimaryKeyException('');
            }
            if (isset(self::$_loading[static::class][$primary])) {
                unset(self::$_loading[static::class][$primary]);
            }
        }

        /**
         * @param WhereGroup $where
         * @param int|null $limit
         * @param int|null $offset
         * @param array $orders
         * @param bool $debug
         * @return static[]|string[]
         * @throws MappingNotFoundException
         * @throws NonExistantObjectException
         * @throws PrimaryKeyException
         */
        public static function where(WhereGroup $where, int $limit = NULL, int $offset = NULL, array $orders = [], bool $debug = false): array
        {
            $qb = (new QueryBuilder(self::getDb()))
                ->select()
                ->col(static::getPrimaryCol(), static::class)->alias(static::getPrimaryParam())
                ->from(static::getTable())->alias(static::class)
                ->where($where)
                ->groupBy(static::getPrimaryParam(), static::class);

            if (isset($limit)) {
                $qb->limit($limit);
            }

            if (isset($offset)) {
                $qb->offset($offset);
            }

            foreach ($orders as $order) {
                if (!isset($order['clause'], $order['dir'])) {
                    continue;
                }
                $qb->orderBy($order['clause'], $order['dir']);
            }

            foreach (static::getMappings() as $param => $mapping) {
                if (($mapping['db']['table'] ?? static::getTable()) !== static::getTable()) {
                    $qb->join($mapping['db']['table'])->alias($param);
                    foreach ($mapping['db']['whereCols'] ?? [] as $whereCol) {
                        $qb->on($param, $whereCol, static::class, static::getPrimaryParam());
                    }
                }
            }
            $sel = $qb->exec($debug);

            if ($debug) {
                return [$sel];
            }

            $objects = [];
            foreach ($sel->fetchAll(PDO::FETCH_ASSOC) as $row) {
                $objects[] = &static::get($row[static::getPrimaryCol()]);
            }

            return $objects;
        }

        /**
         * @param $primaryKey
         * @return static|DataPromise
         * @throws PrimaryKeyException
         */
        public static function &promise($primaryKey)
        {
            if (!in_array(gettype($primaryKey), ['string', 'integer', 'double', 'float'])) {
                throw new PrimaryKeyException('Invalid primary key type');
            }
            if (isset(static::getObjects()[$primaryKey])
                && static::getObjects()[$primaryKey] instanceof static) {
                return static::getObjects()[$primaryKey];
            }
            self::$_objects[static::class][$primaryKey] = new DataPromise(static::class, $primaryKey);

            return self::$_objects[static::class][$primaryKey];
        }

        /**
         * @param $primaryKey
         * @param bool $returnNew
         * @return static|static[]
         * @throws PrimaryKeyException
         * @throws NonExistantObjectException
         * @throws MappingNotFoundException
         */
        public static function &get($primaryKey, bool $returnNew = false)
        {
            if (!in_array(gettype($primaryKey), ['string', 'integer', 'double', 'float'])) {
                throw new PrimaryKeyException('Invalid primary key type');
            }
            if (isset(static::getObjects()[$primaryKey])
                && static::getObjects()[$primaryKey] instanceof static) {
                return static::getObjects()[$primaryKey];
            }
            if (!isset(static::getLoading()[$primaryKey])) {
                static::setLoading($primaryKey);
                try {
                    self::$_objects[static::class][$primaryKey] = (new static())->load($primaryKey);
                } catch (NonExistantObjectException|PrimaryKeyException $e) {
                    unset(self::$_objects[static::class][$primaryKey]);
                    if ($returnNew) {
                        static::unsetLoading($primaryKey);
                        self::$_objects[static::class][$primaryKey] = new static();
                    } else {
                        throw $e;
                    }
                }
                static::unsetLoading($primaryKey);
            }
            return self::$_objects[static::class][$primaryKey];
        }

        /**
         * @return static[]
         * @throws MappingNotFoundException
         * @throws NonExistantObjectException
         * @throws PrimaryKeyException
         */
        public static function &getAll(): array
        {
            $sel = self::getDb()->query(
                'SELECT `' . static::getPrimaryCol() . '` FROM `' . static::getTable() . '`'
            );

            foreach ($sel->fetchAll(\PDO::FETCH_ASSOC) as $row) {
                static::get($row[static::getPrimaryCol()]);
            }

            return static::getObjects();
        }

        /**
         * @param $primaryKey
         * @return bool
         * @throws PrimaryKeyException
         */
        public static function unload($primaryKey): bool
        {
            if (!self::isValidArrayKey($primaryKey)) {
                throw new PrimaryKeyException('');
            }
            if (isset(self::$_objects[static::class][$primaryKey])) {
                unset(self::$_objects[static::class][$primaryKey]);
                return true;
            }
            return false;
        }

        public static function isLoaded($primaryKey): bool
        {
            return isset(self::$_objects[static::class][$primaryKey])
                && self::$_objects[static::class][$primaryKey] instanceof static;
        }

        /**
         * @param $primaryKey
         * @throws Exception
         * @throws PrimaryKeyException
         */
        final public function __construct($primaryKey = null)
        {
            $this->uniq = uniqid(static::class . '#', true);
            if ($primaryKey !== null) {
                $this->load($primaryKey);
            }
            if (method_exists(static::class, 'postConstruct')) {
                $this->postConstruct($primaryKey);
            }
        }

        /**
         * @return mixed
         * @throws PrimaryKeyException
         * @throws MappingNotFoundException
         */
        public function &getPrimary()
        {
            foreach (static::getMappings() as $param => $mapping) {
                if (isset($mapping['primary']) && (bool)(int)$mapping['primary']) {
                    return $this->$param;
                }
            }
            throw new PrimaryKeyException('Primary param not found');
        }

        public function setPrimary($primaryKey): void
        {
            if (debug_backtrace()[1]['class'] !== DataSaver::class) {
                throw new Exception();
            }
            foreach (static::getMappings() as $param => $mapping) {
                if (isset($mapping['primary']) && (bool)(int)$mapping['primary']) {
                    settype($primaryKey, static::getParamType(static::getPrimaryParam()));
                    $this->$param = $primaryKey;
                    return;
                }
            }
        }

        /**
         * @return int|string
         * @throws PrimaryKeyException
         * @throws MappingNotFoundException
         */
        public static function getPrimaryParam()
        {
            foreach (static::getMappings() as $param => $mapping) {
                if (isset($mapping['primary']) && (bool)(int)$mapping['primary']) {
                    return $param;
                }
            }
            throw new PrimaryKeyException('Primary param not found');
        }

        /**
         * @return array|bool
         * @throws PrimaryKeyException
         * @throws MappingNotFoundException
         */
        public static function getPrimaryMapping()
        {
            return static::getMappings(static::getPrimaryParam());
        }

        /**
         * @return string
         * @throws PrimaryKeyException
         * @throws MappingNotFoundException
         */
        public static function getPrimaryCol(): string
        {
            return static::getPrimaryMapping()['db']['col'] ?? static::getPrimaryParam();
        }

        /**
         * @return bool
         * @throws PrimaryKeyException
         * @throws MappingNotFoundException
         */
        public function delete(): bool
        {
            if (method_exists(static::class, 'preDelete')) {
                $this->preDelete();
            }

            $return = (bool)static::getDb()->preparedQuery(
                'DELETE FROM `' . static::getTable() . '` WHERE `' .
                static::getPrimaryCol() . '` = :_primary',
                [':_primary' => $this->getPrimary()]
            );
            foreach (static::getMappings() as $param => $mapping) {
                $type = static::getParamType($param);
                if (!in_array($type, self::TYPES, true)) {
                    if (is_subclass_of($type, self::class)) {
                        if (isset($mapping['deleteTableObject']) && $mapping['deleteTableObject'] === true) {
                            $this->$param->delete();
                        }
                        if (!static::getParamIsArray($param)
                            || ($mapping['db']['table'] ?? NULL) === $type::getTable()) {
                            continue;
                        }
                    }
                }
                if (isset($mapping['db']['table'], $mapping['db']['whereCols'])) {
                    $conditions = array_map(static function ($col) {
                        return "(`{$col}` = :_primary)";
                    }, $mapping['db']['whereCols']);
                    $conditions = implode(' AND ', $conditions);
                    static::getDb()->preparedQuery(
                        "DELETE FROM `{$mapping['db']['table']}` WHERE {$conditions}",
                        [':_primary' => $this->getPrimary()]
                    );
                }
            }

            if (is_array(static::$existsIn)) {
                foreach (static::$existsIn as $exists) {
                    if (empty($exists['class'] ?? 0) || empty($exists['param'] ?? 0)) {
                        continue;
                    }

                    try {
                        /** @var TableObject $class */
                        $class = $exists['class'];
                        $mapping = $class::getMappings($exists['param']);
                        $type = $class::getParamType($exists['param']);

                        if (isset($mapping['db']['table'], $mapping['db']['whereCols'])) {
                            $binds = [':_primary' => $this->getPrimary()];
                            $conditions = [sprintf('(`%s` = :_primary)', $mapping['db']['col'])];

                            if (($type ?? 'string') === 'dynamic'
                                && !empty($mapping['dynamicType']['col'] ?? 0)) {
                                /** @noinspection UnsupportedStringOffsetOperationsInspection */
                                $conditions[] = "(`{$mapping['dynamicType']['col']}` = :_class)";
                                $binds[':_class'] = static::class;
                            }

                            $conditions = implode(' AND ', $conditions);
                            static::getDb()->preparedQuery(
                                "DELETE FROM `{$mapping['db']['table']}` WHERE {$conditions}",
                                $binds
                            );
                        }
                    } catch (MappingNotFoundException $e) {
                    }
                }
            }

            if (method_exists(static::class, 'postDelete')) {
                $this->postDelete();
            }
            return $return;
        }

        /**
         *
         * @param bool $debug
         * @return array|static
         * @throws MappingNotFoundException
         */
        public function save(bool $debug = false)
        {
            if (method_exists(static::class, 'preSave')) {
                $this->preSave();
            }

            $save = (new DataSaver($this))->save($this->getAllMappedParams(), $debug);

            if (method_exists(static::class, 'postSave')) {
                $this->postSave();
            }

            /** @noinspection ProperNullCoalescingOperatorUsageInspection */
            return $save ?? $this;
        }

        /**
         * @param $primaryKey
         * @return $this|bool
         * @throws MappingNotFoundException
         * @throws NonExistantObjectException
         * @throws PrimaryKeyException
         */
        private function load($primaryKey)
        {
            if (!self::isValidArrayKey($primaryKey)) {
                throw new PrimaryKeyException();
            }

            if (method_exists(static::class, 'preLoad')) {
                $this->preLoad();
            }

            foreach ((new DataLoader($this))->load($primaryKey) as $param => $value) {
                $this->$param = $value;
            }

            if (method_exists(static::class, 'postLoad')) {
                $this->postLoad();
            }

            return $this;
        }

        /**
         * @param array $data
         * @return $this
         *
         * @todo: This functionality is not yet finished
         */
        public function setParams(array $data): TableObject
        {
            foreach ($data as $key => $value) {
                if (is_array($value)) {
                    $this->$key = [];
                    foreach ($value as $item) {
                        $this->assign($key, $item);
                    }
                } else {
                    $this->assign($key, $value);
                }
            }
            return $this;
        }

        /**
         * @return string
         */
        public static function getTable(): string
        {
            if (empty(static::$table)) {
                return preg_replace("/.*\\\\(.*)$/", '$1', static::class);
            }
            return static::$table;
        }

        /**
         * @return string
         */
        public static function getClass(): string
        {
            return static::class;
        }

        public static function instanceOf($object, string $class): bool
        {
            if (!is_object($object)) {
                return false;
            }

            if ($object instanceof DataPromise) {
                return $object->getClass() === $class;
            }

            return get_class($object) === $class;
        }

        /**
         * @param string|null $param
         * @return array
         * @throws MappingNotFoundException
         */
        public static function getMappings(string $param = NULL): array
        {
            // Inherit the parent mappings and overwrite and extend them with our own mappings.
            if (!(self::$_mergedMappings[static::class] ?? false)) {
                /** @var TableObject $parent */
                $parent = get_parent_class(static::class);
                static::$mappings = array_replace_recursive($parent::$mappings ?? [], static::$mappings);
                self::$_mergedMappings[static::class] = true;
            }

            if ($param !== NULL) {
                if (isset(static::$mappings[$param])) {
                    return static::$mappings[$param];
                }
                throw new MappingNotFoundException("Mapping not found for ({$param})");
            }
            return static::$mappings;
        }

        /**
         * @param string $param
         * @return string
         * @throws MappingNotFoundException
         */
        public static function getParamType(string $param): string
        {
            $type = static::getMappings($param)['type'] ?? 'string';

            return is_array($type)
                ? $type[0]
                : preg_replace(
                    "/^(?>\[)?([^\]]*)(?>\])?$/",
                    '$1',
                    $type
                );
        }

        /**
         * @param string $param
         * @return bool
         * @throws MappingNotFoundException
         */
        public static function getParamIsArray(string $param): bool
        {
            $type = static::getMappings($param)['type'] ?? 'string';

            return is_array($type)
                || preg_match(
                    "/^\[([^\]]*)\]$/",
                    $type
                );
        }

        /**
         * @param $param
         * @return bool|mixed
         * @throws MappingNotFoundException
         */
        public static function getParamLoadMethods($param)
        {
            $mapping = static::getMappings($param);

            return (isset($mapping['loadMethods'])
                && is_array($mapping['loadMethods']))
                ? $mapping['loadMethods']
                : false;
        }

        /**
         * @param $key
         * @return bool
         */
        public static function isValidArrayKey($key): bool
        {
            return in_array(gettype($key), ['string', 'integer', 'double', 'float']);
        }

        public static function arraySearch(array $array, $object)
        {
            if (!is_object($object) ||
                !(is_subclass_of($object, self::class) || $object instanceof DataPromise)) {
                throw new Exception();
            }

            return array_keys(array_filter(
                $array,
                static function ($var) use ($object) {
                    /** @var TableObject $var */
                    if ($object instanceof DataPromise) {
                        $object = [
                            'class' => $object->getClass(),
                            'primary' => $object->getPromisedKey()
                        ];
                    } else {
                        $object = [
                            'class' => $object::getClass(),
                            'primary' => $object->getprimary()
                        ];
                    }
                    if ($var instanceof DataPromise) {
                        $var = [
                            'class' => $var->getClass(),
                            'primary' => $var->getPromisedKey()
                        ];
                    } else {
                        $var = [
                            'class' => $var::getClass(),
                            'primary' => $var->getprimary()
                        ];
                    }
                    return $object == $var;
                }
            ))[0] ?? false;
        }

        /**
         * @param $array
         * @param mixed ...$objects
         * @return bool|null
         * @throws Exception
         */
        public static function inArray(array $array, ...$objects): ?bool
        {
            foreach ($objects as $object) {
                if (!is_object($object) ||
                    !(is_subclass_of($object, self::class) || $object instanceof DataPromise)) {
                    throw new Exception();
                }
                if (!count(array_filter(
                    $array,
                    static function ($var) use ($object) {
                        /** @var TableObject $var */
                        if ($object instanceof DataPromise) {
                            $object = [
                                'class' => $object->getClass(),
                                'primary' => $object->getPromisedKey()
                            ];
                        } else {
                            $object = [
                                'class' => $object::getClass(),
                                'primary' => $object->getprimary()
                            ];
                        }
                        if ($var instanceof DataPromise) {
                            $var = [
                                'class' => $var->getClass(),
                                'primary' => $var->getPromisedKey()
                            ];
                        } else {
                            $var = [
                                'class' => $var::getClass(),
                                'primary' => $var->getprimary()
                            ];
                        }
                        return $object == $var;
                    }
                ))) {
                    return false;
                }
            }
            return true;
        }

        public static function anyInArray(array $array, ...$objects): ?bool
        {
            foreach ($objects as $object) {
                if (!is_object($object) ||
                    !(is_subclass_of($object, self::class) || $object instanceof DataPromise)) {
                    throw new Exception();
                }
                if (count(array_filter(
                    $array,
                    static function ($var) use ($object) {
                        /** @var TableObject $var */
                        if ($object instanceof DataPromise) {
                            $object = [
                                'class' => $object->getClass(),
                                'primary' => $object->getPromisedKey()
                            ];
                        } else {
                            $object = [
                                'class' => $object::getClass(),
                                'primary' => $object->getprimary()
                            ];
                        }
                        if ($var instanceof DataPromise) {
                            $var = [
                                'class' => $var->getClass(),
                                'primary' => $var->getPromisedKey()
                            ];
                        } else {
                            $var = [
                                'class' => $var::getClass(),
                                'primary' => $var->getprimary()
                            ];
                        }
                        return $object == $var;
                    }
                ))) {
                    return true;
                }
            }
            return false;
        }

        /**
         * @param $object1
         * @param $object2
         * @return bool
         */
        public static function equates($object1, $object2): bool
        {
            if ($object1 === NULL XOR $object2 === NULL) {
                return false;
            }

            if ($object1 instanceof DataPromise) {
                $object1 = [
                    'class' => $object1->getClass(),
                    'primary' => $object1->getPromisedKey()
                ];
            } else {
                $object1 = [
                    'class' => $object1::getClass(),
                    'primary' => $object1->getprimary()
                ];
            }
            if ($object2 instanceof DataPromise) {
                $object2 = [
                    'class' => $object2->getClass(),
                    'primary' => $object2->getPromisedKey()
                ];
            } else {
                $object2 = [
                    'class' => $object2::getClass(),
                    'primary' => $object2->getprimary()
                ];
            }
            return $object1 == $object2;
        }

        /**
         * @return array|false
         * @throws MappingNotFoundException
         */
        final public function getAllMappedParams()
        {
            $mappings = static::getMappings();

            return array_combine(
                array_keys($mappings),
                array_map(
                    function ($param) {
                        return $this->$param ?? NULL;
                    },
                    array_keys($mappings)
                )
            );
        }
    }
}