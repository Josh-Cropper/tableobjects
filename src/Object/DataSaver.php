<?php

namespace TableObjects\Object {

    use Exception;
    use TableObjects\PDOWrapper\QueryBuilder\QueryBuilder;
    use TableObjects\PDOWrapper\QueryBuilder\WhereGroup;

    class DataSaver
    {

        private $_caller;

        public function __construct(TableObject $_caller)
        {
            $this->_caller = $_caller;
        }

        public function save(array $params, bool $debug = false)
        {
            $inserts = [];
            foreach ($this->_caller::getMappings() as $param => $mapping) {
                $type = $this->_caller::getParamType($param);
                $table = $mapping['db']['table'] ?? $this->_caller::getTable();
                $vals = $this->cast($param, $params[$param]);

                $match = false;
                /** @var QueryBuilder[] $inserts */
                foreach ($inserts as $insert) {
                    if ($insert->getFrom()['table'] !== $table) {
                        continue;
                    }
                    if (!$insert->simpleWhereMatch($table, $mapping['db']['whereCols'] ?? [], $this->_caller->getPrimary())) {
                        continue;
                    }

                    $this->bindVals($insert, $vals, $param, $table);

                    $match = true;
                    break;
                }

                if (!$match && !empty($vals)) {
                    $q = (new QueryBuilder($this->_caller::getDb()))->insert()->into($table)->onDuplicateKeyUpdate();
                    $this->bindVals($q, $vals, $param, $table);

                    if (isset($mapping['db']['whereCols'])) {
                        foreach ($mapping['db']['whereCols'] as $whereCol) {
                            $q->where($whereCol, $table, '=', $this->_caller->getPrimary());
                        }
                    }
                    $inserts[] = $q;

                    if (isset($mapping['db']['whereCols'])) {
                        if (is_array($params[$param]) && (!in_array($insert->getFrom(), [$this->_caller::getTable(), $this->_caller::getClass()]))) {
                            $where = (new WhereGroup())
                                ->where(
                                    $mapping['db']['col'] ?? $param,
                                    $table,
                                    'NOT IN',
                                    implode(',', array_map(
                                        function ($var) {
                                            return $var->getPrimary();
                                        },
                                        $params[$param]
                                    ))
                                );
                            foreach ($mapping['db']['whereCols'] as $whereCol) {
//                                var_dump([$whereCol, $table, '=', $this->_caller->getPrimary()]);
                                $where->where($whereCol, $table, '=', $this->_caller);
                            }
//                            die();
                            $inserts[] = (new QueryBuilder($this->_caller::getDb()))
                                ->delete()->from($table)
                                ->where(
                                    $where
                                );
                        }
                    }
                }
            }

            $queries = [];
            $this->_caller::getDb()->beginTransaction();
            foreach ($inserts as $insert) {
                $queries[] = $insert->exec(true);
                if (!$debug) {
                    $sql = $insert->exec();
                    if ($sql->errorCode() !== '00000') {
                        $this->_caller::getDb()->rollBack();
                        throw new Exception("SQL Error ({$sql->errorCode()}): {$sql->errorInfo()[2]}");
                    }

                    if (empty($this->_caller->getPrimary())) {
                        $this->_caller->setPrimary($this->_caller::getDb()->lastInsertId());
                    }
                }

                /**
                 * @todo: Remake deletion for relational saves
                 */
            }
            $this->_caller::getDb()->commit();

            if ($debug) {
                return $queries;
            }
        }

        private function bindVals(QueryBuilder $qb, array $vals, string $param, string $table): void
        {
            $mapping = $this->_caller::getMappings($param);
            foreach ($vals as $k => $val) {
                $qb->col($mapping['db']['col'] ?? $param, $table)->val($val['foreign'], $k);
                if (!empty($mapping['db']['whereCols'][0] ?? NULL)) {
                    $qb->col($mapping['db']['whereCols'][0], $table)->val($val['primary'], $k);
                }
                if ($this->_caller::getParamType($param) === 'dynamic') {
                    if (!isset($mapping['dynamicType']['col']) || empty($mapping['dynamicType']['col'])) {
                        throw new Exception('Dynamic type mapping not set');
                    }
                    $qb->col($mapping['dynamicType']['col'], $table, 'dynamic_col')->val($val['dynamic'], $k);
                }
            }
        }

        private function cast($param, &$value)
        {
            $type = $this->_caller::getParamType($param);
            $is_array = $this->_caller::getParamIsArray($param);
            $mapping = $this->_caller::getMappings($param);

            $rows = [];
            foreach ($is_array ? $value : [$value] as $key => $var) {
                switch ($type) {
                    case 'int':
                    case 'double':
                    case 'float':
                    case 'bool':
                    case 'string':
                        $rows[$key]['foreign'] = $var;
                        continue 2;
                    case 'json':
                        $rows[$key]['foreign'] = NULL;
                        if (is_array($var)) {
                            $rows[$key]['foreign'] = json_encode($var);
                        }
                        continue 2;
                }

                if ($var === NULL) {
                    continue;
                }

                if (method_exists($var, $mapping['saveMethod'] ?? NULL)) {
                    $var = $var->{$mapping['saveMethod']}(
                        ...$mapping['saveArguments'] ?? []
                    );
                    $rows[$key]['foreign'] = $var;
                    continue;
                }

                if($type === \DateTime::class) {
                    if($var === NULL) {
                        $rows[$key]['foreign'] = NULL;
                        continue;
                    }
                    $rows[$key]['foreign'] = $var->format('Y-m-d H:i:s');
                    continue;
                }

                if ($type === 'dynamic' || is_subclass_of($type, TableObject::class)) {
                    if ($var instanceof DataPromise) {
                        $foreign = $var->getPromisedKey();
                        $class = $var->getClass();
                    } else {
                        $foreign = $var->getPrimary();
                        $class = get_class($var);
                    }

                    $rows[$key] = [
                        'foreign' => $foreign
                    ];

                    if (!empty($mapping['db']['whereCols'] ?? 0)) {
                        $rows[$key]['primary'] = &$this->_caller;
                    }
                    if ('dynamic' === $type) {
                        $rows[$key]['dynamic'] = $class;
                    }
                    unset($foreign, $class);
                }
            }

            return $rows;
        }

    }

}