<?php
declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: The_Evil_Dice
 * Date: 13/04/2018
 * Time: 21:42
 */

namespace TableObjects\PDOWrapper {

    use PDO;
    use PDOException;

    /**
     * @method lastInsertId()
     * @method beginTransaction()
     * @method commit()
     * @method rollBack()
     * @method exec(string $string)
     */
    class PDOWrapper
    {

        private $db;

        /**
         * Database constructor.
         * @param $host
         * @param $username
         * @param $passwd
         * @param $database
         * @param $port
         * @param array $options
         * @throws PDOException
         */
        public function __construct($host, $username = null, $passwd = null, $database = null, $port = null, $options = [])
        {
            if($host instanceof PDO) {
                $this->db = $host;
                return;
            }

            $this->db = new PDO("mysql:host={$host};port={$port};dbname={$database}", (string)$username, (string)$passwd, $options);
        }

        public function __call($name, $arguments)
        {
            return $this->db->{$name}(...$arguments);
        }

        /**
         * @param string $preparedStatement
         * @param array $params
         * @return \PDOStatement
         * @throws \Exception
         */
        public function preparedQuery(string $preparedStatement, array $params): \PDOStatement
        {
            $prepare = $this->prepare($preparedStatement);
            $prepare->execute($params);
            return $prepare;
        }

        /**
         * @return bool
         */
        public function ping(): bool
        {
            return (bool)$this->query('SELECT 1')->fetchColumn();
        }

    }
}