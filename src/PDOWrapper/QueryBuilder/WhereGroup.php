<?php

namespace TableObjects\PDOWrapper\QueryBuilder {

    use TableObjects\Object\TableObject;

    class WhereGroup {

        public const AND = 'AND';
        public const OR = 'OR';
        public const XOR = 'XOR';

        public const TYPE_WHERE = 'where';
        public const TYPE_WHEREGROUP = 'whereGroup';

        public const VALID_OPERATORS = [
            '=' => ':col: = :bind:',
            '<' => ':col: < :bind:',
            '>' => ':col: > :bind:',
            '!=' => ':col: != :bind:',
            '<=' => ':col: <= :bind:',
            '>=' => ':col: >= :bind:',
            'LIKE' => ':col: LIKE :bind:',
            'NOT LIKE' => ':col: NOT LIKE :bind:',
            'IN' => 'FIND_IN_SET(:col:,:bind:)',
            'FIND_IN_SET' => 'FIND_IN_SET(:col:,:bind:)',
            'NOT IN' => 'NOT FIND_IN_SET(:col:,:bind:)',
            'NOT FIND_IN_SET' => 'NOT FIND_IN_SET(:col:,:bind:)'
        ];

        private $_relation;
        private $_wheres = [];

        public function __construct(string $_relation = self::AND)
        {
            $this->_relation = $_relation;
        }

        public function where($col, string $from = NULL, string $operator = NULL, $value = NULL)
        {
            if($col instanceof self) {
                $this->_wheres[] = [
                    'type' => self::TYPE_WHEREGROUP,
                    'value' => $col
                ];
            } elseif(is_string($col) && isset($from,$operator,$value)) {
                if(!isset(self::VALID_OPERATORS[$operator])) {
                    throw new \Exception("Invalid Operator {$operator}");
                }
                $this->_wheres[] = [
                    'type'     => self::TYPE_WHERE,
                    'col'      => $col,
                    'from'     => $from,
                    'operator' => $operator,
                    'value'    => $value
                ];
            }
            return $this;
        }

        public function exec(?array &$binds = [])
        {
            $exec = implode(" {$this->_relation} ", array_map(
                static function ($where) use (&$binds) {
                    switch($where['type']) {
                        case self::TYPE_WHERE:
                            $bind = ':_' .(!empty($where['from']) ? str_replace('\\','_',"{$where['from']}_") : '') . uniqid("{$where['col']}_", false);
                            $binds[$bind] = (is_object($where['value']) && method_exists($where['value'], 'getPrimary'))
                                ? $where['value']->getPrimary()
                                : $where['value'];
                            return preg_replace(
                                ['/:col:/','/:bind:/'],
                                [
                                    (!empty($where['from']) ? "`{$where['from']}`." : '') .
                                    "`{$where['col']}`",
                                    $bind
                                ],
                                self::VALID_OPERATORS[$where['operator']]
                            );
                        case self::TYPE_WHEREGROUP:
                            return $where['value']->exec($binds);
                    }
                    return NULL;
                },
                $this->_wheres
            ));

            if(!empty(trim($exec,'()'))) {
                return "({$exec})";
            }
            return $exec;
        }
    }

}