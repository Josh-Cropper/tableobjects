<?php

namespace TableObjects\PDOWrapper\QueryBuilder {

    use Exception;
    use TableObjects\Object\DataPromise;
    use TableObjects\Object\TableObject;
    use TableObjects\PDOWrapper\PDOWrapper;

    class QueryBuilder
    {
        /**
         * @var PDOWrapper $_db
         */
        private $_db;

        /* Type Constants */
        public const INSERT = 'INSERT';
        public const SELECT = 'SELECT';
        public const COUNT = 'SELECT COUNT(*)';
        public const DELETE = 'DELETE';
        public const UPDATE = 'UPDATE';

        /* Join Constants */
        public const LEFT_JOIN = 'LEFT JOIN';
        public const RIGHT_JOIN = 'RIGHT JOIN';
        public const INNER_JOIN = 'INNER JOIN';
        public const OUTER_JOIN = 'OUTER JOIN';

        /* Order Constants */
        public const ORDER_ASC = 'asc';
        public const ORDER_DESC = 'desc';

        /* State Tracking Properties */

        /**
         * @var string $_lastCalled
         */
        private $_lastCalled;

        /**
         * @var bool $_initialised
         */
        private $_initialised = false;

        /* Query Building Properties */

        private $_type = self::SELECT;
        private $_cols = [];
        private $_rows = [];
        private $_table;
        private $_joins = [];
        private $_wheres = [];
        private $_relation = WhereGroup:: AND;
        private $_groups = [];
        private $_limit;
        private $_offset;
        private $_orders = [];
        private $_options = [
            'onDuplicateKeyUpdate' => false
        ];

        /**
         * QueryBuilder constructor.
         * @param PDOWrapper $_db
         */
        public function __construct(PDOWrapper $_db)
        {
            $this->_db = $_db;
        }

        /**
         * @param string $type
         * @param string ...$cols
         * @return QueryBuilder
         * @throws Exception
         */
        private function setType(string $type, string ...$cols): QueryBuilder
        {
            if (!self::supportedQueryType($type)) {
                throw new Exception('Query type not supported');
            }
            if ($this->_initialised) {
                throw new Exception("{$type} must be called first");
            }

            $this->_initialised = true;
            $this->_lastCalled = 'init';
            $this->_type = $type;
            foreach ($cols as $col) {
                $this->_cols[] = ['col' => $col];
            }
            return $this;
        }

        /**
         * @param string ...$cols
         * @return QueryBuilder
         * @throws Exception
         */
        public function select(string ...$cols): QueryBuilder
        {
            return $this->setType(self::SELECT, ...$cols);
        }

        /**
         * @param string ...$cols
         * @return QueryBuilder
         * @throws Exception
         */
        public function count(): QueryBuilder
        {
            return $this->setType(self::COUNT);
        }

        /**
         * @param string $table
         * @return QueryBuilder
         * @throws Exception
         */
        public function update(string $table): QueryBuilder
        {
            return $this->setType(self::UPDATE)->from($table);
        }

        /**
         * @return QueryBuilder
         * @throws Exception
         */
        public function insert(): QueryBuilder
        {
            return $this->setType(self::INSERT);
        }

        /**
         * @return QueryBuilder
         * @throws Exception
         */
        public function delete(): QueryBuilder
        {
            return $this->setType(self::DELETE);
        }

        /**
         * @param string $col
         * @param string|null $from
         * @param string|null $alias
         * @return QueryBuilder
         * @throws Exception
         */
        public function col(string $col, string $from = null, string $alias = null): QueryBuilder
        {
            if ($this->_type === self::DELETE) {
                throw new Exception('Cols cannot be specified on delete queries');
            }
            $key = (!empty($from ?? NULL) ? "{$from}." : '') . $col;
            $this->_cols[$key] = [
                'col'   => $col,
                'from'  => $from,
                'alias' => $alias
            ];
            $this->_lastCalled = "col[{$key}]";
            return $this;
        }

        public function orderBy(string $clause, string $dir = self::ORDER_ASC)
        {
            $this->_orders[] = [
                'clause' => $clause,
                'dir'    => ($dir === self::ORDER_DESC)
                    ? self::ORDER_DESC
                    : self::ORDER_ASC
            ];
        }

        public function limit(int $limit)
        {
            if ($limit < 0) {
                throw new Exception('Limit must not be less than 0');
            }
            $this->_limit = $limit;
            return $this;
        }

        public function offset(int $offset)
        {
            if ($offset < 0) {
                throw new Exception('Offset must not be less than 0');
            }
            $this->_offset = $offset;
            return $this;
        }

        /**
         * @param $value
         * @param int $row
         * @return QueryBuilder
         * @throws Exception
         */
        public function val(&$value, int $row = 0): QueryBuilder
        {
            if (!preg_match('/col\[(.+)]/', $this->_lastCalled, $col)) {
                throw new Exception('Val must be called directly after a col declaration');
            }
            if(is_bool($value)) {
                $value = (int)$value;
            }
            $this->_rows[$row][$col[1]] = $value;
            return $this;
        }

        /**
         * @param string $table
         * @return QueryBuilder
         */
        public function from(string $table): QueryBuilder
        {
            $this->_table = ['table' => $table];
            $this->_lastCalled = 'table';
            return $this;
        }

        public function getFrom()
        {
            return $this->_table;
        }

        /**
         * @param string $table
         * @return QueryBuilder
         */
        public function into(string $table): QueryBuilder
        {
            return $this->from($table);
        }

        public function where($col, string $from = NULL, string $operator = NULL, $value = NULL)
        {
            if ($col instanceof WhereGroup) {
                $this->_wheres[] = [
                    'type'  => 'whereGroup',
                    'value' => $col
                ];
            } elseif (is_string($col) && isset($from, $operator, $value)) {
                $this->_wheres[] = [
                    'type'     => 'where',
                    'col'      => $col,
                    'from'     => $from,
                    'operator' => $operator,
                    'value'    => $value
                ];
            }
            return $this;
        }

        public function whereType(string $type = WhereGroup:: AND)
        {
            $this->_relation = $type;
            return $this;
        }

        /**
         * @param $col
         * @param string|NULL $from
         * @param string|NULL $operator
         * @param null $value
         * @return bool
         */
        public function hasWhere($col = NULL, string $from = NULL, string $operator = NULL, $value = NULL): bool
        {
            if (is_string($col) && isset($from, $operator, $value)) {
                return (bool)count(array_filter($this->_wheres, function ($where) use ($value, $operator, $from, $col) {
                    return ($where['type'] !== 'whereGroup')
                        ? ($where['col'] === $col) && ($where['from'] === $from)
                        && ($where['operator'] === $operator) && ($where['value'] === $value)
                        : false;
                }));
            }
            if ($col === NULL) {
                return empty($this->_wheres);
            }
            return false;
        }

        public function onlyWhere($col, string $from = NULL, string $operator = NULL, $value = NULL)
        {
            if (is_string($col) && isset($from, $operator, $value)) {
                return 1 === count(array_filter($this->_wheres, function ($where) use ($value, $operator, $from, $col) {
                        return ($where['type'] !== 'whereGroup')
                            ? ($where['col'] === $col) && ($where['from'] === $from)
                            && ($where['operator'] === $operator) && ($where['value'] === $value)
                            : false;
                    }));
            }
        }

        /**
         * @param string $table
         * @param array $cols
         * @param $bind
         * @return bool
         */
        public function simpleWhereMatch(string $table, array $cols, $bind): bool
        {
            return count($cols) === count(
                    array_filter(
                        $cols,
                        function ($whereCol) use ($table, $bind) {
                            return $this->hasWhere($whereCol, $table, '=', $bind);
                        }
                    )
                );
        }

        /**
         * @param string|null $type
         * @return array
         */
        private function getWheres(string $type = NULL): array
        {
            switch ($type) {
                case WhereGroup::TYPE_WHERE:
                case WhereGroup::TYPE_WHEREGROUP:
                    return array_filter($this->_wheres, static function ($where) use ($type) {
                        return $where['type'] === $type;
                    });
                default:
                    return $this->_wheres;
            }
        }

        public function checkWhere(array $wheres = [])
        {

            if (count($this->getWheres(WhereGroup::TYPE_WHEREGROUP))
                || (count($wheres) !== count($this->getWheres(WhereGroup::TYPE_WHERE)))) {
                return false;
            }
            return true;
        }

        /**
         * @param string $table
         * @param string $type
         * @param array[] $ons
         * @return QueryBuilder
         * @throws Exception
         */
        public function join(string $table, string $type = self::LEFT_JOIN, array ...$ons): QueryBuilder
        {
            $this->_joins[] = [
                'table'    => $table,
                'joinType' => $type,
                'ons'      => $ons
            ];
            $this->_lastCalled = 'join';
            return $this;
        }

        /**
         * @param string $foreignTable
         * @param string $foreignColumn
         * @param string $localTable
         * @param string $localColumn
         * @return QueryBuilder
         * @throws Exception
         */
        public function on(string $foreignTable, string $foreignColumn, string $localTable,
                           string $localColumn): QueryBuilder
        {
            if ($this->_lastCalled !== 'join') {
                throw new Exception('On must be called directly after join or alias');
            }
            $this->_joins[count($this->_joins) - 1]['ons'][] = [
                'localTable'    => $localTable,
                'localColumn'   => $localColumn,
                'foreignTable'  => $foreignTable,
                'foreignColumn' => $foreignColumn
            ];
            return $this;
        }

        /**
         * @param string $alias
         * @return QueryBuilder
         * @throws Exception
         */
        public function alias(string $alias): QueryBuilder
        {
            switch ($this->_lastCalled) {
                case 'table':
                    $this->_table['alias'] = $alias;
                    break;
                case 'join':
                    $this->_joins[count($this->_joins) - 1]['alias'] = $alias;
                    break;
                default:
                    if (preg_match('/col\[(.+)]/', $this->_lastCalled, $col)) {
                        $this->_cols[$col[1]]['alias'] = $alias;
                        return $this;
                    }

                    throw new Exception('Alias must be called directly after select, update, into, from or join calls');
            }
            return $this;
        }

        /**
         * @param $ons
         * @return bool
         */
        private function joinOnCheck($ons): bool
        {
            if (empty($ons)) {
                return false;
            }
            foreach ($ons as $on) {
                if (!isset($on['localTable'], $on['localColumn'], $on['foreignTable'], $on['foreignColumn'])) {
                    return false;
                }
            }
            return true;
        }

        public function onDuplicateKeyUpdate()
        {
            if ($this->_type !== self::INSERT) {
                throw new Exception('onDuplicateKeyUpdate can only be used with insert');
            }
            $this->_options['onDuplicateKeyUpdate'] = true;
            return $this;
        }

        public function groupBy(string $col, string $from = NULL): QueryBuilder
        {
            if (!in_array(['col' => $col, 'from' => $from], $this->_groups, true)) {
                $this->_groups[] = [
                    'col'  => $col,
                    'from' => $from
                ];
            }

            return $this;
        }

        /**
         * @param string $type
         * @return bool
         */
        private static function supportedQueryType(string $type): bool
        {
            return in_array(
                $type,
                [
                    self::SELECT,
                    self::COUNT,
                    self::UPDATE,
                    self::INSERT,
                    self::DELETE
                ],
                true
            );
        }

        private function getRowVals(array $row, int $k = 0, array &$binds)
        {
            return '(' . implode(', ', array_map(static function ($val, $col) use (&$binds, $k) {
                    if ($val instanceof TableObject) {
                        $val = $val->getPrimary();
                    } elseif ($val instanceof DataPromise) {
                        $val = $val->getPromisedKey();
                    }
                    $bind = sprintf(':_%s', uniqid(str_replace('.', '_', "{$col}_"),false));
                    $binds[$bind] = $val;
                    return $bind;
                }, $row, array_keys($row))) . ')';
        }

        /**
         * @param bool $returnQueryString
         * @return array|\PDOStatement|string|null
         * @throws Exception
         */
        public function exec(bool $returnQueryString = false)
        {
            if (!self::supportedQueryType($this->_type)) {
                throw new Exception('Query type not supported');
            }
            $query = [
                'type'  => $this->_type,
                'limit' => isset($this->_limit)
                    ? (isset($this->_offset)
                        ? "LIMIT {$this->_offset}, {$this->_limit}"
                        : "LIMIT {$this->_limit}")
                    : NULL,
                'order' => !empty($this->_orders)
                    ? 'ORDER BY ' . implode(', ', array_map(
                        static function ($order) {
                            return "{$order['clause']} {$order['dir']}";
                        }
                        , $this->_orders))
                    : ''
            ];
            $binds = [];
            switch ($this->_type) {
                case self::COUNT:
                    $query['pre_from'] = 'FROM';
                    $query['groups'] = $this->_groups
                        ? 'GROUP BY ' . implode(', ', array_map(static function ($col) {
                            return (!empty($col['from']) ? "`{$col['from']}`." : '') . "`{$col['col']}`";
                        }, $this->_groups)) : '';
                    break;
                case self::SELECT:
                    $query['pre_from'] = 'FROM';
                    $query['cols'] = implode(', ', array_map(static function ($col) {
                        return (!empty($col['from']) ? "`{$col['from']}`." : '') .
                            "`{$col['col']}`" .
                            (!empty($col['alias']) ? " AS `{$col['alias']}`" : '');
                    }, $this->_cols));
                    $query['groups'] = $this->_groups
                        ? 'GROUP BY ' . implode(', ', array_map(static function ($col) {
                            return (!empty($col['from']) ? "`{$col['from']}`." : '') . "`{$col['col']}`";
                        }, $this->_groups)) : '';
                    break;
                case self::UPDATE:
                    $query['post_from'] = 'SET';
                    $query['cols'] = implode(', ', array_map(
                        function ($col) use (&$binds) {
                            $binds[":_{$col['from']}_{$col['col']}"] = $this->_rows[0][$col['col']];
                            return (!empty($col['from']) ? "`{$col['from']}`." : '') .
                                "`{$col['col']}` = :_{$col['from']}_{$col['col']}";
                        }, $this->_cols));
                    break;
                case self::INSERT:
                    $query['pre_from'] = 'INTO';
                    $query['cols'] = '(' .
                        implode(', ',
                            array_map(static function ($col) {
                                return (!empty($col['from']) ? "`{$col['from']}`." : '') . "`{$col['col']}`";
                            }, $this->_cols)) .
                        ') VALUES ' . implode(', ', array_map(
                            function ($row, $k) use (&$binds) {
                                return $this->getRowVals($row, $k, $binds);
                            }, $this->_rows, array_keys($this->_rows)
                        ));
                    $query['on_duplicate'] = $this->_options['onDuplicateKeyUpdate']
                        ? 'ON DUPLICATE KEY UPDATE ' . implode(', ', array_map(static function ($col) {
                            return (!empty($col['from']) ? "`{$col['from']}`." : '') .
                                "`{$col['col']}` = VALUES(" . (!empty($col['from']) ? "`{$col['from']}`." : '') . "`{$col['col']}`)";
                        }, $this->_cols))
                        : '';
                    break;
                case self::DELETE:
                    $query['pre_from'] = 'FROM';
                    break;
            }
            $query['from'] = "`{$this->_table['table']}`" .
                (!empty($this->_table['alias']) ? " AS `{$this->_table['alias']}`" : '');
            switch ($this->_type) {
                case self::SELECT:
                case self::COUNT:
                case self::UPDATE:
                case self::INSERT:
                    $query['joins'] = implode("\n", array_map(function ($join) {
                        if (!$this->joinOnCheck($join['ons'])) {
                            throw new Exception('Join Exception');
                        }
                        return "{$join['joinType']} `{$join['table']}`" . (!empty($join['alias']) ? " AS `{$join['alias']}`" : '') . " ON " .
                            implode(' AND ', array_map(static function ($on) {
                                return "`{$on['foreignTable']}`.`{$on['foreignColumn']}` = `{$on['localTable']}`.`{$on['localColumn']}`";
                            }, $join['ons']));
                    }, $this->_joins));
                    if ($this->_type !== self::INSERT && !empty(array_filter($this->_wheres, function ($where) {
                        if($where['type'] === 'whereGroup') {
                            return !empty($where['value']->exec());
                        }
                        return true;
                    }))) {
                        $query['wheres'] = 'WHERE (' . implode(" {$this->_relation} ", array_map(
                                static function ($where) use (&$binds) {
                                    switch ($where['type']) {
                                        case 'where':
                                            $bind = ':_' .(!empty($where['from']) ? str_replace('\\','_',"{$where['from']}_") : '') . uniqid("{$where['col']}_", false);
                                            $binds[$bind] = $where['value'];
                                            return (!empty($where['from']) ? "`{$where['from']}`." : '') .
                                                "`{$where['col']}` " . "{$where['operator']} {$bind}";
                                        case 'whereGroup':
                                            return $where['value']->exec($binds);
                                    }
                                    return NULL;
                                },
                                $this->_wheres
                            )) . ')';
                    }
                    break;
                case self::DELETE:
                    if (!empty($this->_wheres)) {
                        $query['wheres'] = 'WHERE (' . implode(" {$this->_relation} ", array_map(
                                static function ($where) use (&$binds) {
                                    switch ($where['type']) {
                                        case 'where':
                                            return (!empty($where['from']) ? "`{$where['from']}`." : '') .
                                                "`{$where['col']}` " . "{$where['operator']} " . $where['value'];
                                        case 'whereGroup':
                                            return $where['value']->exec($binds);
                                    }
                                    return NULL;
                                },
                                $this->_wheres
                            )) . ')';
                    }
                    break;
                default:
                    return null;
            }
            $order = [];
            switch ($this->_type) {
                case self::COUNT:
                    $order = ['type', 'pre_from', 'from', 'joins', 'wheres', 'groups', 'order', 'limit'];
                    break;
                case self::SELECT:
                    $order = ['type', 'cols', 'pre_from', 'from', 'joins', 'wheres', 'groups', 'order', 'limit'];
                    break;
                case self::UPDATE:
                    $order = ['type', 'from', 'post_from', 'cols', 'wheres', 'order', 'limit'];
                    break;
                case self::INSERT:
                    $order = ['type', 'pre_from', 'from', 'cols', 'on_duplicate'];
                    break;
                case self::DELETE:
                    $order = ['type', 'pre_from', 'from', 'wheres', 'order', 'limit'];
                    break;
            }
            $query = array_filter($query, static function ($key) use ($order) {
                return in_array($key, $order, true);
            }, ARRAY_FILTER_USE_KEY);
            uksort($query, static function ($a, $b) use ($order) {
                return (int)array_search($a, $order, true)
                > (int)array_search($b, $order, true) ? 1 : -1;
            });
            if ($returnQueryString) {
                return [
                    'query' => implode(' ', $query),
                    'binds' => $binds,
                    'rows' => $this->_rows
                ];
            }
            return $this->_db->preparedQuery(implode(' ', $query), $binds);
        }
    }
}