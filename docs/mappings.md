## Mappings 
 
*The mappings is multidimensional array of instructions on how your object should behave and interact with your table.* 

Items in the array are sets of string keys, being your property names, to arrays. 
Such as: 
```
    protected static $mappings = [
        'id' => [
            'primary' => true,
            'type' => 'int'
        ],
        'name' => []
    ]
```

Possible Options Are:
* primary 
    - Requires bool
    - Defines this property as the primary key for this object's table
* type
    - Requires string or array
    - Defaults to "string"
    - Valid Options: int|double|float|string|bool|json|dynamic|ClassName Or \[ClassName\]
    - Specifies how the value should be handled when being loaded and saved. Using either dynamic or \[ClassName\] indicates that this is a one to many relationship, dynamic being polymorphic.
* dynamicType
    - col
        - Required only is type set to dynamic
        - Requires string
        - Specify the column in which the type of the polymorphic relationship is stored.
* allowNull
    - Requires bool
    - Defaults to false
* saveMethod
    - Optional
    - Requires string
    - Method name to call on this property prior to saving, the return of this method is what will be saved
* saveArguments
    - Optional
    - Requires array
    - Arguments passed by splat operator to the saveMethod if specified
* db (Need To Complete Documentation)
    - table
    - col
    - whereCols
    - orderBy