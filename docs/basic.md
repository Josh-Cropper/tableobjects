## Basic Setup

1. Create a class that extends TableObjects\TableObject
    ```
    namespace Example {
    
        class Example extends TableObject\TableObject
        {
        
        }
    
    }
    ```
    
1. If you don't define your table, TableObjects will assume your classname as your table name
    ```
        class Example extends TableObject\TableObject
        {
        
            protected static $table = 'example';
        
        }
    ```

1. Define your parameters according to the columns in your table
    ```
        class Example extends TableObject\TableObject
        {
        
            protected static $table = 'example';
            
            protected $id;
            protected $name;
            protected $price;
            protected $quantity;
            protected $closingDate;
        
        }
    ```

1. Now we will define our parameter mappings, here we will only use a few basic mappings features, for a more in depth overview of all the features, please see the mappings documentation.
    ```
            protected static $mappings = [
                'id' => [
                    'primary' => true,
                    'type' => 'int'
                ],
                'name' => [],
                'price' => [
                    'type' => 'double'
                ],
                'quantity' => [
                    'type' => 'int'
                ],
                'closingDate' => [
                    'type' => \DateTime::class,
                    'db' => [
                        'col' => 'closing_date'
                    ]
                ]
            ]
    ``` 
   As you can see, certain keys only need to be provided in certain circumstances; 'type' defaults to string, 'primary' defaults to false and 'col' defaults to the property name.
1. The last thing to do for this class is to add the getters and setters. This won't be needed if you've made your parameters public, though that is not advised for a multitude of reasons which I won't cover here.

    In this instance, getId() will not be required, as all TableObjects inherit the getPrimary() function with returns the parameter defined as primary in the mappings.
    ```
            public function getName(): string
            {
                return $this->name;
            }
            
            public function setName(string $name): Example
            {
                $this->name = $name;
                return $this;
            }
            
            public function getPrice(): double
            {
                return $this->price;
            }
            
            public function setPrice(double $price): Example
            {
                $this->price = $price;
                return $this;
            }
            
            public function getQuantity(): int
            {
                return $this->quantity;
            }
            
            public function setQuantity(string $quantity): Example
            {
                $this->quantity = $quantity;
                return $this;
            }
    ```

1. Finally, let's initialise our connection to the database
    ```
    TableObject::init(
        new TableObjects\PDOWrapper\PDOWrapper(
            $host,
            $username,
            $passwd,
            $database,
            $port,
            $options
        )
    );
    ``` 
   You can also simply provide a basic PDO Object if desired