<?php

use TableObjects\Object\TableObject;
use TableObjects\PDOWrapper\QueryBuilder\WhereGroup;
use TestModel1\TestModel1;
use TestModel2\TestModel2;

spl_autoload_register(function ($className) {
    if(preg_match('/^TableObjects\\\\/', $className)) {
        $className = preg_replace("/^TableObjects\\\\(.*)/", '$1', $className);
    }
    if(is_readable(__DIR__ . '/../../src/' . preg_replace("/\\\\/", '/', $className) . '.php')) {
        include_once __DIR__ . '/../../src/' . preg_replace("/\\\\/", '/', $className) . '.php';
    } else {
        include_once __DIR__ . '/../../tests/test1/' . preg_replace("/\\\\/", '/', $className) . '.php';
    }
});

TableObject::init(new PDO(
    "mysql:host=localhost;dbname=test;", 'root', ''
));

var_dump(TestModel1::get(3)->getParent());