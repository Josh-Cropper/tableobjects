<?php

namespace TestModel1 {

    use TableObjects\Object\DataPromise;
    use TableObjects\Object\TableObject;
    use TestModel2\TestModel2;

    class TestModel1 extends TableObject
    {
        protected $id;
        protected $string;
        protected $parent;
        protected $testModel2s = [];

        protected static $mappings = [
            'id' => [
                'primary' => true,
                'type' => 'int'
            ],
            'string' => [],
            'parent' => [
                'type' => self::class,
                'db' => [
                    'col' => 'parent_id'
                ]
            ],
            'testModel2s' => [
                'type' => [TestModel2::class],
                'db' => [
                    'table' => 'testmodel1_testmodel2_links',
                    'col' => 'testmodel2_id',
                    'whereCols' => [
                        'testmodel1_id'
                    ]
                ]
            ]
        ];

        public function setString(string $string): self
        {
            $this->string = $string;

            return $this;
        }

        /**
         * @return array
         */
        public function getTestModel2s(): array
        {
            return $this->testModel2s;
        }

        /**
         * @param TestModel2 $testModel2
         * @return TestModel1
         */
        public function addTestModel2(TestModel2 $testModel2): self
        {
            $this->testModel2s[] = $testModel2;

            return $this;
        }

        /**
         * @return TestModel1|NULL
         */
        public function getParent(): ?TestModel1
        {
            if($this->parent instanceof DataPromise) {
                $this->parent = $this->parent->fulfill();
            }
            return $this->parent;
        }
    }

}