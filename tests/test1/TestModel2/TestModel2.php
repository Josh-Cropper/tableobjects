<?php

namespace TestModel2 {

    use TableObjects\Object\TableObject;
    use TestModel1\TestModel1;

    class TestModel2 extends TableObject
    {
        protected $id;
        protected $time;

        protected static $mappings = [
            'id' => [
                'primary' => true,
                'type' => 'int'
            ],
            'time' => [
                'type' => \DateTime::class,
                'saveMethod' => 'format',
                'saveArguments' => [
                    'Y-m-d H:i:s'
                ]
            ]
        ];

        protected static $existsIn = [
            [
                'class' => TestModel1::class,
                'param' => 'testModel2s'
            ]
        ];

        public function preSave()
        {
            if(!($this->time instanceof \DateTime)) {
                $this->time = new \DateTime();
            }
        }
    }

}